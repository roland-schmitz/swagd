#!/bin/bash
set -e

# get swig 1.3.40 source
wget https://sourceforge.net/projects/swig/files/swig/swig-1.3.40/swig-1.3.40.tar.gz;

# quietly untar
tar xzf swig-1.3.40.tar.gz;

# make swig
echo 'Making SWIG 1.3.40...';

cd swig-1.3.40 && ./configure && make -j10 && make install;
