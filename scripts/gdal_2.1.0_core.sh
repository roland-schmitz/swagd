#!/bin/sh
set -e

# get gdal from osgeo
wget http://download.osgeo.org/gdal/2.1.0/gdal-2.1.0.tar.gz;

# untar the source quietly
tar xzf gdal-2.1.0.tar.gz;

# configure and make/install gdal
echo 'Making GDAL...';
cd gdal-2.1.0 && ./configure --without-libtool && make -j10 && make install;
