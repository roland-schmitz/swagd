#!/bin/bash

# Reference gradle.properties in this repository to ensure all values agree.

# You MUST have GPG keys created AND published to a public keystore in order to
# sign artifacts! See https://www.gnupg.org/gph/en/manual/x457.html

USER_HOME=/root
# For any other container user:
# USER_HOME=/home/containerUser

docker run --rm \
    # The key you have stored in ~/.gnupg gets mounted to the user
    # home directory so gradle can publish with it.
    -v ~/.gnupg/:$USER_HOME/.gnupg \
    # This is optional: if you want to store gradle artifacts in a docker
    # volume named "gradle_home", then mount it to the container in the
    # appropriate place.
    -v gradle_home:$USER_HOME/.gradle \
    # Mount your CURRENT WORKING DIRECTORY to /workspace in gdalwebapp-docker
    # for swagd, the root repo directory is where you should execute this
    # script.
    -v "$PWD":/workspace \
    reinventinggeospatial/gdalwebapp-docker:latest \
    ./gradlew clean publish
